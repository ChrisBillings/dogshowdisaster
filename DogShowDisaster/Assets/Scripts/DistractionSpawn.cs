﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DistractionSpawn : MonoBehaviour
{
    // Start is called before the first frame update
    public Transform[] spawnPoints;
    public float spawnTime = 3f;
    public GameObject distraction;

    void Start()
    {
        
        Invoke("Spawn", spawnTime);
        
    }

    // Update is called once per frame
    void Spawn()
    {
        // if the player hasn't lost:
        if (!GameObject.Find("Game Manager").GetComponent<GameManager>().gameOver & GameObject.Find("Game Manager").GetComponent<GameManager>().pauseSpawning)
        {
            // find random spawnpoint
            int spawnPointIndex = Random.Range(0, spawnPoints.Length);

            // instantiate distraction at the spawnpoint
            GameObject d = Instantiate(distraction, spawnPoints[spawnPointIndex].position, distraction.transform.rotation);
            // tag the new distraction with "Distraction"
            d.tag = "Distraction";
            // if the spawntime is above the bottom threshhold:
            if (spawnTime > .5f)
            {
                // increases difficulty by decreasing amount of time between spawns
                spawnTime -= .2f;
            }

            // calls the method again, with the new spawn time.
            Invoke("Spawn", spawnTime);
        }
    }
}
