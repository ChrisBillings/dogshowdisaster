﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeployDistraction : MonoBehaviour
{
    public GameObject distractionPrefab;
    public float respawnTime = 1.0f;
    private Vector3 screenBounds;
    Vector3 newPosition;
    
    void Start()
    {
        screenBounds = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width, Screen.height, Camera.main.transform.position.z));
        StartCoroutine(distractionSpawn());
        
    }
    private void spawnDistraction()
    {
        //new Vector3(screenBounds.x * -2, Random.Range(-screenBounds.x, screenBounds.x));
        GameObject d = Instantiate(distractionPrefab) as GameObject;
        //d.transform.position = new Vector3(screenBounds.x * -2, Random.Range(-screenBounds.x, screenBounds.x));
    }
    IEnumerator distractionSpawn()
    {
        while(true)
        {
            yield return new WaitForSeconds(respawnTime);
            spawnDistraction();
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
