﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DistractingDogs : MonoBehaviour
{
    //public GameObject[] doggies;
    int randomDog;
    GameObject distractedDog;
    

    // Start is called before the first frame update
    void Start()
    {
        //doggies = (GameObject.FindGameObjectsWithTag("Doggy"));
        //int randomDog = Random.Range(0, GameObject.Find("Game Manager").GetComponent<GameManager>().doggies.Length);
        //distractedDog = GameObject.Find("Game Manager").GetComponent<GameManager>().doggies[randomDog];
        //distractedDog.GetComponent<DogMovement>().distracted = true;
        distract();
    }
    

    // Update is called once per frame
    void Update()
    {
        // if the game is not over:
        if (!GameObject.Find("Game Manager").GetComponent<GameManager>().gameOver)
        {
            // the dog will moves toward the distraction
            distractedDog.transform.position = Vector3.MoveTowards(distractedDog.transform.position, gameObject.transform.position, distractedDog.GetComponent<DogMovement>().moveSpeed * Time.deltaTime);
            if (!distractedDog.GetComponent<DogMovement>().distractable)
            {
                // if the dog leaves, remove the distraction
                Destroy(gameObject);
            }
        }
    }

    void distract()
    {
        // finds random dog
        int randomDog = Random.Range(0, GameObject.Find("Game Manager").GetComponent<GameManager>().doggies.Length);
        distractedDog = GameObject.Find("Game Manager").GetComponent<GameManager>().doggies[randomDog];
        // if the random dog can be distracted and is not already distracted:
        if (distractedDog.GetComponent<DogMovement>().distractable == true & !distractedDog.GetComponent<DogMovement>().distracted)
        {
            // make it distracted, decrease the amount of time needed to leave next time it gets distracted
            distractedDog.GetComponent<DogMovement>().distracted = true;
            distractedDog.GetComponent<DogMovement>().helperFloat += .01f;
        }
        else
        {
            // if the random dog can't be distracted/is already distracted, find a different dog
            distract();
        }

    }

    private void OnDestroy()
    {
        // makes the dog distracted
        distractedDog.GetComponent<DogMovement>().distracted = false;
    }
}
