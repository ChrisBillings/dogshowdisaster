﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public GameObject[] doggies;
    public GameObject[] distractions;
    public int numberLeft;
    public bool gameOver = false;
    public GameObject GameOverImage;
    public bool pauseSpawning = false;
    // Start is called before the first frame update
    void Start()
    {
        //sets up the array of the game objects representing the show dogs
        distractions = (GameObject.FindGameObjectsWithTag("Distraction"));
        doggies = (GameObject.FindGameObjectsWithTag("Doggy"));
    }

    // Update is called once per frame
    void Update()
    {
        distractions = (GameObject.FindGameObjectsWithTag("Distraction"));
        //After more than 2 dogs have left, lose the game
        if (Input.GetKey(KeyCode.Return))
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        }
        if (distractions.Length < 5)
        {
            pauseSpawning = true;
        }
        else
        {
            pauseSpawning = false;
        }
        if (numberLeft > 2)
        {
            lose();
        }
    }
    public void lose()
    {
        //this variable is used to disable other scripts when the game is over
        gameOver = true;
        //enable the game over image
        GameOverImage.SetActive(true);

    }
}
