﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BarkController : MonoBehaviour
{
    public float speed;
    public GameObject enemy;
    public float timer = 0.5f;
    //public float zoom;
   

    // Start is called before the first frame update
    void Start()
    {
        //Destroy the bullet after half a second
        Invoke("despawn", timer);
    }

    // Update is called once per frame
    void Update()
    {
        //Move the bullet
        //transform.Translate(Vector3.forward * speed * Time.deltaTime);
       //Code that was given commented, presumably doesn't work
       // zoom += Time.deltaTime; //sets the timer to destruction 
       //if (zoom > 3)
       //  self.destruct;


     
    }

    private void OnTriggerEnter(Collider other)
    {
        //Check if the bullet has collided with a distraction
        if(other.gameObject.tag == "Distraction")
        {
            //Destroy both this and the distraction
            Destroy(this.gameObject);
            Destroy(other.gameObject);
        }

    }
    private void despawn()
    {
        //destroy this bullet
        Destroy(this.gameObject);
    }
}
