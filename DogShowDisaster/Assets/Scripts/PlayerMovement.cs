﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    Rigidbody rigidBody;
    public float speed = 1.5f;
    Vector3 lookPos;
    public BarkPointerController theBark;


    void Start()
    {
        rigidBody = GetComponent<Rigidbody>();
    }

    private void Update()
    {
        if (!GameObject.Find("Game Manager").GetComponent<GameManager>().gameOver)
        {
            //controls which way the plkkayer is facing/aiming
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;
            if (Physics.Raycast(ray, out hit, 100))
            {
                lookPos = hit.point;
            }
            Vector3 lookDir = lookPos - transform.position;
            lookDir.y = 0;
            transform.LookAt(transform.position + lookDir, Vector3.up);

            if (Input.GetMouseButtonDown(0))
            {
                theBark.isFiring = true; //if pressing left mouse, then fire

            }
            if (Input.GetMouseButtonUp(0))
            {
                theBark.isFiring = false; //if left mouse button released, then don't fire anymore
            }
        }

    }



    void FixedUpdate()//move the player
    {
        if (!GameObject.Find("Game Manager").GetComponent<GameManager>().gameOver)
        {
            float horizontal = Input.GetAxis("Horizontal");
            float vertical = Input.GetAxis("Vertical");

            Vector3 movement = new Vector3(horizontal, 0, vertical);

            rigidBody.AddForce(movement * speed / Time.deltaTime);
        }
    }
}
