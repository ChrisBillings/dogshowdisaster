﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DogMovement : MonoBehaviour
{
    //Using serialize fields to allow all the dogs to be on different movement paths?
    [SerializeField]
    Transform[] waypoints;
    

    [SerializeField]
    public float moveSpeed = 5f;
    public bool distracted = false;
    int waypointIndex = 0;
    float distractedTimer = 5f;
    public bool distractable = true;
    public float helperFloat = 0f;
    //public List<GameObject> distraction;


    void Start()
    {
        transform.position = waypoints[waypointIndex].transform.position; //sets dog's initial position
        //distraction = new List<GameObject>();
    }

    void Update()
    {
        if (!GameObject.Find("Game Manager").GetComponent<GameManager>().gameOver)
        {
            if (!distracted & distractable)
            {
                Move();
                distractedTimer = 7f - helperFloat;
            }
            if (distracted)
            {
                distractedTimer -= Time.deltaTime;
            }
            if (distractedTimer < 0 & distractable)
            {
                leave();
            }
        }
    }

    void Move()
    {
        //moves the dog towards the spot it is supposed to go
        transform.position = Vector3.MoveTowards(transform.position, waypoints[waypointIndex].transform.position, moveSpeed * Time.deltaTime);

        //when the dog reaches a waypoint
        if (transform.position == waypoints[waypointIndex].transform.position)
        {
            waypointIndex += 1; //target the next waypoint
        }

        if (waypointIndex == waypoints.Length)//if you reach the last waypoint
            waypointIndex = 0;//reset to the first waypoint

    }
    void leave()
    {
        distractable = false;
        gameObject.transform.position = new Vector3(-100, -100, -100);
        GameObject.Find("Game Manager").GetComponent<GameManager>().numberLeft += 1;
    }


}