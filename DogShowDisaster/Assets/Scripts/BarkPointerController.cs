﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BarkPointerController : MonoBehaviour
{
    //variables
    public bool isFiring;
    public BarkController bark;
    public float barkSpeed = 10;
    public float timeBetweenShots;
    private float shotCounter;
    public Transform firePoint;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if(isFiring)//checks if firing
        {
            shotCounter -= Time.deltaTime;//sets shot fire delay rate
            if(shotCounter <= 0)
            {
                shotCounter = timeBetweenShots;//sets up delay between barks

                BarkController newBark = Instantiate(bark, firePoint.position, firePoint.rotation) as BarkController;//fires
                newBark.speed = barkSpeed;//sets speed of bark
            }
        }
        
    }
}
